# Use Python 3.8 slim image as base
FROM python:3.8-slim

# Set the working directory inside the container
WORKDIR /app

# Copy the requirements file to the container
COPY requirements.txt .

# Install Flask and other dependencies
RUN pip install -r requirements.txt

# Copy the rest of the application code to the container
COPY . .

# Expose the port on which the Flask app runs (default is 5000)
EXPOSE 5000

# Command to run the Flask application
CMD ["python", "app.py"]
